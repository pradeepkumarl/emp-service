<%@page import="java.util.List, com.hexaware.ems.model.Employee"  %>

<html>
  <body>
      <h1> List of All Employees: </h1> 
      <ul>
         <% 
             List<Employee> employees = (List<Employee>) request.getAttribute("employees"); 
               for (Employee empl: employees){
                 %>
                 <li>
                 <%
                 out.write(empl.getName());
               }%></li>
      </ul>
  </body>
</html>