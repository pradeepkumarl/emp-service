package com.hexaware.ems.service;

import java.util.Arrays;
import java.util.List;

import com.hexaware.ems.model.Employee;

public class EmployeeServiceImple implements EmployeeService {

	@Override
	public List<Employee> getEmployees() {
		List<Employee> employees = Arrays.asList(
					new Employee(12, "Vikas"),
					new Employee(13, "Ram"),
					new Employee(14, "Harini"),
					new Employee(15, "Usha"));
		
		return employees;
	}

}
