package com.hexaware.ems.service;

import java.util.List;

import com.hexaware.ems.model.Employee;

public interface EmployeeService {

	List<Employee> getEmployees();

}
