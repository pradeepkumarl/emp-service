package com.hexaware.ems.client;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hexaware.ems.model.Employee;
import com.hexaware.ems.service.EmployeeService;
import com.hexaware.ems.service.EmployeeServiceImple;

public class EmployeeServlet extends HttpServlet {
	
	private EmployeeService employeeService = new EmployeeServiceImple();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		List<Employee> employees = employeeService.getEmployees();
		RequestDispatcher dispatcher = request.getRequestDispatcher("emplist.jsp");
		request.setAttribute("employees", employees);
		dispatcher.forward(request, response);
		
		
		
//		PrintWriter writer = response.getWriter();
//		writer.println("<html><body>");
//		writer.println("<ul>");
//		
//		
//		Iterator<Employee> it = employees.iterator();
//		while(it.hasNext()) {
//			writer.println("<li>" + it.next().getName()+"</li>");
//		}
//		writer.println("</ul>");
//		writer.println("</body></html>");
	}

}
